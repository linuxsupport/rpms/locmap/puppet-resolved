class resolved () {
  class { 'systemd':
    manage_resolved   => true,
    use_stub_resolver => true,
    fallback_dns      => '',
    llmnr             => false,
  }
  # Disable nscd if it's currently defined
  service {'nscd':
    ensure => stopped,
    enable => false,
  }
}
