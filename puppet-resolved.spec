Name:           puppet-resolved
Version:        1.2
Release:        1%{?dist}
Summary:        Puppet module for resolved

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch

Requires:       puppet-agent
Requires:       puppet-systemd
%if 0%{?rhel} >= 8
Obsoletes:      puppet-nscd <= 2.4
%endif

%description
Puppet module for systemd-resolved

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/resolved/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/resolved/
touch %{buildroot}/%{_datadir}/puppet/modules/resolved/linuxsupport

%files -n puppet-resolved
%{_datadir}/puppet/modules/resolved

%post
MODULE=$(echo %{name} | cut -d \- -f2)
if [ -f %{_datadir}/puppet/modules/${MODULE}/linuxsupport ]; then
  grep -qE "autoreconfigure *= *True" /etc/locmap/locmap.conf && AUTORECONFIGURE=1 || :
  if [ $AUTORECONFIGURE ]; then
    locmap --list |grep $MODULE |grep -q enabled && MODULE_ENABLED=1 || :
    if [ $MODULE_ENABLED ]; then
      echo "locmap autoreconfigure enabled, running: locmap --configure $MODULE"
      locmap --configure $MODULE || :
    else
      echo "locmap autoreconfigure enabled, however the $MODULE module is not enabled"
      echo "Skipping (re)configuration of $MODULE"
    fi
  fi
fi

%changelog
* Tue Oct 08 2024 Ben Morrice <ben.morrice@cern.ch> 1.2-1
- Add autoreconfigure %post script

* Tue Oct 10 2023 Ben Morrice <ben.morrice@cern.ch> 1.1-2
- Change naming to puppet-resolved

* Thu Oct 05 2023 Ben Morrice <ben.morrice@cern.ch> 1.1-1
- Tweak code

* Thu Oct 05 2023 Ben Morrice <ben.morrice@cern.ch> 1.0-2
- Obsolete puppet-nscd

* Thu Oct 05 2023 Ben Morrice <ben.morrice@cern.ch> 1.0-1
- Initial release
